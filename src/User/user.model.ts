import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import User from './user.interface';
const saltRounds = 10;

//Define a schema
const Schema = mongoose.Schema;

const userSchema = new Schema({
  firstname: {
    type: String,
    required: [true, 'Firstname is required'],
    match: [/^[a-zA-Z0-9-^~]{1,50}$/, 'Invalid Name'],
  },
  lastname: {
    type: String,
    // required: [true, 'Lastname is required'],
    match: /[a-zA-Z]/,
  },
  email: {
    type: String,
    required: [true, 'Email is required'],
    unique: true,
    lowercase: true,
    trim: true,
    match: [/(^[a-zA-Z0-9._]*)+@rishabhsoft+\.com$/, 'Invalid email'],
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    trim: true,
    match: [
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/,
      'Invalid password',
    ],
  },
  role: {
    type: Schema.Types.ObjectId,
    ref: 'Role',
  },
  loginAttempt: {
    type: Number,
    default: 0,
  },
  lastLoggedInAt: {
    type: Number,
    default: null
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});
// Hash user password before saving into database
userSchema.pre<User & mongoose.Document>('save', function(next) {
  let user = this;
  user.password = bcrypt.hashSync(user.password, saltRounds);
  console.log(user.password);
  next();
});

const UserModel = mongoose.model<User & mongoose.Document>('User', userSchema);

export default UserModel;
