import { errorHandler, detectError } from '../utils/handler';
import { i18N } from './../utils/i18N';
import express, { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
const saltRounds = 10;
import bcrypt from 'bcrypt';

// reposne handler
import { responseHandler } from '../utils/handler';

// interfaces for controller
import Controller from '../common/controller.interface';
// import Role from '../Roles/role.interface';

// models
import UserModel from './user.model';
import RoleModel from '../Roles/role.model';
import VerifyToken from '../middleware/verifyToken';

class UserController implements Controller {
  public path = '/user';
  public router = express.Router();

  // database models
  private User = UserModel;
  private Role = RoleModel;

  constructor() {
    this.initializeRoutes();
  }
  
  public initializeRoutes(): void {
    this.router.post(`${this.path}/register`, this.addUser);
    this.router.post(`${this.path}/newpassword`, this.newPassword);
    this.router.get(`${this.path}`, VerifyToken, this.getUser);
  }
  
  // register/add new user
  public addUser = async (req: Request, res: Response) => {
    try {
      if (req.body.password !== req.body.confirmPassword) {
        let payload = {
          status: 401,
          message: i18N.errorMessages.matchingPasswords,
          data: {},
        };
        errorHandler(res, payload);
      } else {
        let defaultRole = await this.Role.findOne({ name: 'User' }, { _id: 1 });
        let newUser = new this.User({
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          email: req.body.email,
          password: req.body.password,
          role: defaultRole,
        });
        await newUser.save();
        let payload = {
          status: 200,
          message: i18N.successMessages.registerSuccess,
          data: {},
        };
        responseHandler(res, payload);
      }
    } catch (error) {
      detectError(error, res);
    }
  };
  
  // new password
  public newPassword = async (req: Request, res: Response, next) => {
    try {
      let token = req.headers.authorization.split(' ')[1];
      if (!token) {
        let payload = {
          status: 403,
          message: i18N.errorMessages.tokenError,
          error: {},
        };
        errorHandler(res, payload);
      }
      let vtoken = jwt.verify(token, process.env.SECRET);
      if (req.body.password !== req.body.confirmPassword) {
        let payload = {
          status: 401,
          message: i18N.errorMessages.matchingPasswords,
          data: {},
        };
        errorHandler(res, payload);
      } else {
        let user = vtoken.userId;
        let resetPassword = bcrypt.hashSync(req.body.password, saltRounds);
        await UserModel.findByIdAndUpdate(
          user,
          { password: resetPassword },
          { new: true }
        );
        let payload = {
          status: 200,
          message: i18N.successMessages.resetPasswordSuccess,
          data: {},
        };
        responseHandler(res, payload);
      }
    } catch (error) {
      let payload = {
        status: 500,
        message: error.Messages,
        error: error,
      };
      errorHandler(res, payload);
    }
  };

  public getUser = async (req: Request, res: Response) => {
    try {
      const user = await UserModel.findById(res.locals.userId);
      if (!user) {
        let payload = {
          status: 400,
          message: i18N.errorMessages.noAvailbleRecords,
          data: {},
        };
        errorHandler(res, payload);
      } else {
        let UserData = {
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email,
          role: user.role,
        };
        let payload = {
          status: 200,
          message: i18N.successMessages.dataFound,
          data: UserData,
        };
        responseHandler(res, payload);
      }
    } catch (error) {
      detectError(error, res);
    }
  };
}

export default UserController;
