export default interface User {
  _id?: String;
  firstname: String;
  lastname: String;
  email: String;
  password: String;
  role?: { _id: string; role: string };
  logiAttempt?: Number;
  lastLoggedInAt?: Number;
  createdAt?: Date;
  joinedDate?: Date;
}
