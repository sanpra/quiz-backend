import { errorHandler } from './../utils/handler';
import { i18N } from './../utils/i18N';
import UserModel from '../User/user.model';
import jwt from 'jsonwebtoken';

// verify token middleware

const VerifyToken = async (req, res, next) => {
  try {
    let token = req.headers.authorization.split(' ')[1];
    if (!token) {
      let payload = {
        status: 401,
        message: i18N.errorMessages.unauthorised,
        error: {},
      };
      return errorHandler(res, payload);
    }
    let userId = await checkExistingSession(token, res);
    if (userId) {
      res.locals.userId = userId;
      next();
    } else {
      let payload = {
        status: 403,
        message: i18N.errorMessages.sessionExpired,
        error: {},
      };
      errorHandler(res, payload);
    }
  } catch (error) {
    let payload = {
      status: 401,
      message: i18N.errorMessages.unauthorised,
      error: {},
    };
    errorHandler(res, payload);
  }
};

const checkExistingSession = async (accessToken, res) => {
  try {
    let token = jwt.verify(accessToken, process.env.SECRET);
    let userId = token.userId;
    const user = await UserModel.findById(
      { _id: userId },
      { _id: 0, lastLoggedInAt: 1 }
    );
    if (token.iat >= user.lastLoggedInAt) {
      return userId;
    } else {
      return false;
    }
  } catch (error) {
    let payload = {
      status: 403,
      message: i18N.errorMessages.sessionExpired,
      error: {},
    };
    errorHandler(res, payload);
  }
};

export default VerifyToken;
