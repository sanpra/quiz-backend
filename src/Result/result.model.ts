import mongoose from 'mongoose';
import { Result } from './result.interface';
const Schema = mongoose.Schema;

const UserAnswerSchema = new Schema({
  answer: {
    type: String,
    trim: true,
    default: null,
  },
  isValid: {
    type: Boolean,
    default: false,
  },
  questionId: {
    type: 'ObjectId',
    ref: 'Question',
  },
});

const ResultSchema = new Schema(
  {
    userScore: {
      type: Number,
      required: [true, 'Score is required'],
      default: 0,
    },
    userPercentage: {
      type: Number,
      required: [true, 'Percentage is required'],
      min: 0,
      max: 100,
      default: 0,
    },
    timeTaken: {
      type: Number,
      required: [true, 'Time is required'],
      min: 0,
      max: 60,
      default: 0,
    },
    result: {
      type: String,
      enum: ['Passed', 'Failed'],
      default: 'Failed',
    },
    userAnswers: {
      type: [UserAnswerSchema],
      required: true,
    },
    quizId: {
      type: Schema.Types.ObjectId,
      ref: 'Quiz',
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    status: {
      type: String,
      enum: ['Started', 'Submitted'],
      required: true,
      default: 'Started',
    },
  },
  { timestamps: true }
);

const ResultModel = mongoose.model<Result & mongoose.Document>('Result', ResultSchema);

export default ResultModel;
