import express, { Request, Response } from 'express';
import mongoose from 'mongoose';
import _ from 'lodash';
import Controller from '../common/controller.interface';
import VerifyToken from '../middleware/verifyToken';
import { responseHandler, errorHandler, detectError } from '../utils/handler';
import { i18N } from './../utils/i18N';
// models
import QuestionModel from '../Quiz/questions.model';
import ResultModel from './result.model';
import { Result, UserAnswer } from './result.interface';
import QuizModel from '../Quiz/quiz.model';

class ResultController implements Controller {
  public path = '/result';
  public router = express.Router();

  // database models
  private Question = QuestionModel;
  private Result = ResultModel;
  private Quiz = QuizModel;

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes(): void {
    this.router.post(`${this.path}/submit`, VerifyToken, this.submitQuiz);
    this.router.get(`${this.path}/:quizId`, VerifyToken, this.getResult);
  }

  public submitQuiz = async (req: Request, res: Response) => {
    try {
      let userResponse = req.body;
      let request = req.body.quizAnswers;
      let userResponseQuizId;
      if (userResponse.quizId) {
        userResponseQuizId = mongoose.Types.ObjectId(userResponse.quizId);
      } else {
        let payload = {
          status: 400,
          message: i18N.errorMessages.invalidQuizId,
        };
        return responseHandler(res, payload);
      }
      let userResponseUserId = mongoose.Types.ObjectId(res.locals.userId);
      let result: Result;

      let status: number;
      let message: string;

      //Find result with given quizId and userId
      const resultData = await this.Result.findOne(
        {
          quizId: userResponseQuizId,
          userId: userResponseUserId,
        },
        { status: 1, _id: 0 }
      );

      //If no result found, set quizId and userId
      if (!resultData) {
        result = new this.Result();
        result.userId = res.locals.userId;
        result.quizId = req.body.quizId;

        status = 200;
        message = i18N.successMessages.addOperationSuccess;
      }
      //else (if result found) calculate score based on result status
      else {
        //if resultData.status is 'Started', calculate result
        if (resultData.status === i18N.resultStatus.Started) {
          const questionData = await this.Question.find(
            { quizId: userResponseQuizId },
            { _id: 1, options: 1 }
          );
          let resultAnswers: Array<UserAnswer> = [];
          let count: number = 0;
          _.map(questionData, q => {
            const tempObj = {
              answer: null,
              isValid: false,
              questionId: null,
            };
            let quesObj = _.find(request, r => q._id == r.questionId);
            const corrObj = _.find(q.options, o => {
              if (quesObj) return o.text === quesObj.answer;
            });
            if (corrObj && corrObj.isCorrect) {
              count++;
            }
            tempObj.answer = quesObj ? quesObj.answer : null;
            tempObj.isValid = corrObj ? corrObj.isCorrect : false;
            tempObj.questionId = q._id;
            resultAnswers.push(tempObj);
          });
          const percentage = Number(
            (
              (count * Number(process.env.MAXPERCENTAGE)) /
              questionData.length
            ).toFixed(2)
          );
          let resultStatus;
          if (percentage > Number(process.env.PASSINGPERCENTAGE)) {
            resultStatus = i18N.successMessages.Pass;
          } else {
            resultStatus = i18N.errorMessages.Fail;
          }

          //final result
          result = {
            userId: res.locals.userId,
            quizId: userResponse.quizId,
            userAnswers: resultAnswers,
            userScore: count,
            status: i18N.resultStatus.Submitted,
            userPercentage: percentage,
            timeTaken: userResponse.timeTaken,
            result: resultStatus,
          };

          status = 200;
          message = i18N.successMessages.resultSubmitSuccess;
        }
        //else (if resultData.status is 'Submitted')
        else {
          let payload = {
            status: 400,
            message: i18N.errorMessages.resultAlreadySubmittted,
          };
          return errorHandler(res, payload);
        }
      }

      //save result to database, and create a new document for result if it does not exist
      result = await this.Result.findOneAndUpdate(
        {
          quizId: userResponseQuizId,
          userId: userResponseUserId,
        },
        result,
        {
          upsert: true,
          new: true,
          projection: { userAnswers: 0, _id: 0, userId: 0 },
        }
      );
      const payload = {
        status: status,
        message: message,
        data: result,
      };
      responseHandler(res, payload);
    } catch (error) {
      detectError(error, res);
    }
  };

  public getResult = async (req: Request, res: Response) => {
    try {
      let resQuizId = new mongoose.Types.ObjectId(req.params.quizId);

      let quizStatus: any = await this.Quiz.findOne(
        { _id: resQuizId },
        { status: 1, _id: 0, title: 1 }
      );
      if (quizStatus.status === 'Expired') {
        let resUserId = new mongoose.Types.ObjectId(res.locals.userId);
        let result;

        result = await this.Question.aggregate([
          { $match: { quizId: resQuizId } },
          { $project: { title: 1, options: 1 } },
          {
            $lookup: {
              from: 'results',
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$quizId', resQuizId] },
                        { $eq: ['$userId', resUserId] },
                      ],
                    },
                  },
                },
                {
                  $project: {
                    userAnswers: 1,
                    _id: 0,
                  },
                },
              ],
              as: 'resultdata',
            },
          },
        ]);

        _.map(result, r => {
          if (r.resultdata[0]) {
            const userAns = r.resultdata[0].userAnswers;
            const userAnswers = _.find(userAns, uA => {
              return r._id.toString() == uA.questionId.toString();
            });
            _.map(r.options, a => {
              a.userSelected = false;
              if (userAnswers.answer == a.text) {
                return (a.userSelected = true);
              }
            });
            delete r.resultdata;
          } else {
            delete r.resultdata;
          }
        });
        let payload = {
          status: 200,
          message: i18N.successMessages.quizResultSuccess,
          data: { quizTitle: quizStatus.title, result },
        };
        responseHandler(res, payload);
      } else {
        let payload = {
          status: 400,
          message: i18N.errorMessages.quizAvailable,
          data: null,
        };
        responseHandler(res, payload);
      }
    } catch (error) {
      detectError(error, res);
    }
  };
}
export default ResultController;
