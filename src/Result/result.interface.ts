export interface Result {
  userScore: Number;
  userPercentage: Number;
  timeTaken: Number;
  result: String;
  userAnswers: Array<UserAnswer>;
  quizId: String;
  userId: String;
  status: String;
}

export interface UserAnswer {
  answer: String;
  isValid: Boolean;
  questionId: String;
}
