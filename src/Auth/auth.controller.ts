import VerifyToken from '../middleware/verifyToken';
import express from 'express';
import { Request, Response } from 'express';
import UserModel from '../User/user.model';
import jwt from 'jsonwebtoken';
import { ValidatePassword } from '../utils/_helper';
import User from 'User/user.interface';
import { i18N } from './../utils/i18N';
import { errorHandler, responseHandler, detectError } from '../utils/handler';
import nodemailer from 'nodemailer';
import handlebars from 'handlebars';
import fs from 'fs';

class AuthController {
  public path = '/auth';
  public userPath = '/user';
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes = () => {
    this.router.post(this.path, this.login);
    this.router.put(`${this.userPath}/:id`, this.logout);
    this.router.post(`${this.path}/reqresetpassword`, this.resetPassword);
    this.router.get(
      `${this.path}/validatingtoken/:token`,
      this.validPasswordToken
    );
  };

  // login
  public login = async (req: Request, res: Response, next) => {
    try {
      const userdata = req.body;
      let user: User = await await UserModel.findOne({
        email: userdata.email,
      }).populate('role', 'name');
      if (!user) {
        let payload = {
          status: 401,
          message: i18N.errorMessages.unregisteredData,
          data: {},
        };
        errorHandler(res, payload);
      } else {
        const validPassword = await ValidatePassword(
          userdata.password,
          user.password
        );
        if (!validPassword) {
          let payload = {
            status: 401,
            message: i18N.errorMessages.unregisteredData,
            data: {},
          };
          errorHandler(res, payload);
        } else {
          let accessToken = jwt.sign(
            { userId: user._id, roleId: user.role._id },
            process.env.SECRET,
            {
              expiresIn: process.env.TOKEN_EXPRIES,
            },
          );
          let getToken = jwt.decode(accessToken);
          await UserModel.findByIdAndUpdate(
            user._id,
            { lastLoggedInAt: getToken.iat },
            { new: true }
          );
          let data = {
            _id: user._id,
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            role: user.role,
            accessToken,
          };
          let payload = {
            status: 200,
            message: i18N.successMessages.loginSuccess,
            data: data,
          };
          responseHandler(res, payload);
        }
      }
    } catch (error) {
      detectError(error, res);
    }
  };

  // Logout controller function
  public logout = async (req: Request, res: Response, next) => {
    try {
      await UserModel.findByIdAndUpdate(
        { _id: req.params.id },
        { lastLoggedInAt: null },
        { new: true }
      );

      let payload = {
        status: 200,
        message: i18N.successMessages.logoutSuccess,
        data: {},
      };
      responseHandler(res, payload);
    } catch (error) {
      let payload = {
        status: 500,
        message: i18N.errorMessages.serverError,
        error: {},
      };
      errorHandler(res, payload);
    }
  };

  public resetPassword = async (req: Request, res: Response, next) => {
    try {
      const users = req.body;
      let user: User = await UserModel.findOne({ email: users.email });

      if (!user) {
        let payload = {
          status: 401,
          message: i18N.errorMessages.unregisteredEmail,
          data: {},
        };
        errorHandler(res, payload);
      }
      let accessToken = jwt.sign({ userId: user._id }, process.env.SECRET, {
        expiresIn: '1h',
      });

      let readHTMLFile = function(path, callback) {
        fs.readFile(path, { encoding: 'utf-8' }, function(err, html) {
          if (err) {
            callback(err);
            throw err;
          } else {
            callback(null, html);
          }
        });
      };

      let transporter = nodemailer.createTransport({
        host: process.env.NODEMAILER_HOST, // hostname
        secureConnection: process.env.NODEMAILER_SECURECONNECTION,
        port: process.env.NODEMAILER_PORT,
        requireTLS: process.env.NODEMAILER_REQUIRE_TLS,
        tls: {
          rejectUnauthorized: process.env.NODEMAILERR_TLS_REJECT_UNAUTHORIZED,
          ciphers: process.env.NODEMAILERR_TLS_CIPHERS,
        },
        auth: {
          user: process.env.NODEMAILER_AUTH_USER,
          pass: process.env.NODEMAILER_AUTH_PASS,
        },
      });

      readHTMLFile(__dirname + './../templates/reqrestpassword.html', function(
        err,
        html
      ) {
        var template = handlebars.compile(html);
        var replacements = {
          url: process.env.REQRESETPASSWORD + accessToken,
          username: user.firstname + ' ' + user.lastname
        };
        var htmlToSend = template(replacements);
        var mailOptions = {
          from: process.env.NODEMAILER_SEND_MAIL_FROM,
          to: user.email,
          subject: process.env.NODEMAILER_SEND_MAIL_SUBJECT,
          html: htmlToSend,
        };

        let info = transporter.sendMail(mailOptions, function(error, response) {
          if (error) {
            throw error;
          }
        });
      });
      let payload = {
        status: 200,
        message: i18N.successMessages.emailSentSuccess,
        data: {},
      };
      responseHandler(res, payload);
    } catch (error) {
      let payload = {
        status: 500,
        message: error.Messages,
        error: error,
      };
      errorHandler(res, payload);
    }
  };

  // Token Validation controller function
  public validPasswordToken = async (req: Request, res: Response, next) => {
    try {
      let token = req.params.token;
      if (!token) {
        let payload = {
          status: 403,
          message: i18N.errorMessages.tokenError,
          error: {},
        };
        errorHandler(res, payload);
      }
      let vtoken = jwt.verify(token, process.env.SECRET);
      let payload = {
        status: 200,
        message: i18N.successMessages.tokenSuccess,
        data: {},
      };
      responseHandler(res, payload);
    } catch (error) {
      let payload = {
        status: 500,
        message: i18N.errorMessages.serverError,
        error: error,
      };
      errorHandler(res, payload);
    }
  };
}

export default AuthController;
