import payload from 'common/payload.interface';
import { i18N } from './i18N';
export const errorHandler = (res, payload: payload) => {
  res.send(payload);
};

export const responseHandler = (res, payload: payload) => {
  res.send(payload);
};

export const detectError = (error, res) => {
  if (error.code === 11000) {
    let payload = {
      status: 400,
      message: i18N.errorMessages.duplicateTitle,
      error: {},
    };
    errorHandler(res, payload);
  } else if (error.name === 'ValidationError') {
    let payload = {
      status: 400,
      message: i18N.errorMessages.validationError,
      error: error,
    };
    errorHandler(res, payload);
  } else {
    let payload = {
      status: 500,
      message: i18N.errorMessages.serverError,
      error: error,
    };
    errorHandler(res, payload);
  }
};
