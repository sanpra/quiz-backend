export const i18N = {
  errorMessages: {
    duplicateField: 'Duplicate insertion of data',
    validationError: 'Invalid or missing parameter',
    serverError: 'Internal server error',
    matchingPasswords: "Password don't match",
    unregisteredData: 'Invalid email or password',
    unregisteredEmail: 'Invalid email',
    tokenError: 'No token provided',
    duplicateTitle: 'Duplicate title for the quiz',
    minimumDataError: 'There must be atleast minimum 15 questions',
    unauthorised: 'Unauthorized user',
    sessionExpired: 'Session expired',
    noAvailbleRecords: 'No records available',
    noQuizAvailble: 'No quiz available',
    noQuestionsAvailable: 'No questions available',
    quizAlreadySubmittted: 'Quiz is already submitted',
    resultAlreadySubmittted: 'Result is already submitted',
    quizAvailable: 'Quiz is not expired yet',
    Fail: 'Failed',
    invalidQuizId: 'Invalid QuizId',
  },
  successMessages: {
    registerSuccess: 'User registered successfully',
    loginSuccess: 'Successfully logged in',
    logoutSuccess: 'Successfully logged out',
    resetPasswordSuccess: 'User password has been changed successfully',
    emailSentSuccess: 'Email to reset password sent successfully',
    tokenSuccess: 'Valid Token',
    addOperationSuccess: 'Added successfully',
    dataFound: 'Data found',
    getData: 'Data found',
    quizSubmitSuccess: 'Quiz submitted successfully',
    resultSubmitSuccess: 'Result submitted successfully',
    alreadyAttempt: 'You have already attempted',
    quizResultSuccess: 'Result found',
    Pass: 'Passed',
  },
  resultStatus: {
    Started: 'Started',
    Submitted: 'Submitted',
  },
};
