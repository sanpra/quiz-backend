import bcrypt from 'bcrypt';

export const ValidatePassword = async (plainPassword, hashedPassword) => {
    try { return await bcrypt.compare(plainPassword, hashedPassword) }
    catch (error) {
        console.log(error)
    }
}