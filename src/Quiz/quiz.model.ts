import mongoose from 'mongoose';
import Quiz from './quiz.interface';

const Schema = mongoose.Schema;

const QuizSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, 'Quiz title is required'],
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    passingPercentage: {
      type: Number,
      default: 60,
      min: 0,
      max: 100,
    },
    endDate: {
      type: Date,
      default: null,
    },
    status: {
      type: String,
      enum: ['Created', 'Available', 'Expired'],
      default: 'Available',
    },
    duration: {
      type: Number,
      min: 0,
      max: 60,
      default: 10,
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  { timestamps: true }
);

const QuizModel = mongoose.model<Quiz & mongoose.Document>('Quiz', QuizSchema);

export default QuizModel;
