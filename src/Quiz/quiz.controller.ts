import { Question } from './questions.interface';
import mongoose from 'mongoose';
import express, { Request, Response } from 'express';

import { i18N } from './../utils/i18N';

// middlewares
import verifyToken from '../middleware/verifyToken';
// reposne handler
import { responseHandler, errorHandler, detectError } from '../utils/handler';
// interfaces
import Controller from '../common/controller.interface';
// models
import QuizModel from './quiz.model';
import QuestionModel from './questions.model';
import ResultModel from '../Result/result.model';

class QuizController implements Controller {
  public path = '/quiz';
  public router = express.Router();

  // database models
  private Quiz = QuizModel;
  private Questions = QuestionModel;
  private Result = ResultModel;
  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes(): void {
    this.router.get(`${this.path}`, verifyToken, this.getQuiz);
    this.router.post(`${this.path}`, verifyToken, this.addQuiz);
    this.router.get(
      `${this.path}/questions/:quizId`,
      verifyToken,
      this.getQuestions
    );
  }

  public addQuiz = async (req: Request, res: Response) => {
    try {
      let newQuiz = new this.Quiz(req.body);
      newQuiz.createdBy = res.locals.userId;

      let questions = req.body.questions;
      let totalQuestions = questions.length;

      // change it to totalQuestions > 15
      if (totalQuestions > process.env.TOTAL_QUESTIONS) {
        questions.forEach(question => {
          question.quizId = newQuiz._id;
        });

        await newQuiz.save();
        await this.Questions.insertMany(questions);

        let payload = {
          status: 200,
          message: i18N.successMessages.addOperationSuccess,
          data: {},
        };
        responseHandler(res, payload);
      } else {
        let payload = {
          status: 400,
          message: i18N.errorMessages.minimumDataError,
          data: {},
        };
        errorHandler(res, payload);
      }
    } catch (error) {
      detectError(error, res);
    }
  };

  public getQuiz = async (req: Request, res: Response) => { 
    try {
      let pageNumber = Number(req.query.pageNumber) || Number(process.env.DEFAULT_PAGE_NUMBER);
      let recordsPerPage = Number(req.query.recordsPerPage) || Number(process.env.RECORDS_PER_PAGE);
      let title = req.query.title;
      let resUserId = new mongoose.Types.ObjectId(res.locals.userId);
      let totalRecords = await this.Quiz.find().count();
      let totalPages = Math.ceil(totalRecords / recordsPerPage);
      let quiz;

      //if user has searched for particular record, then return documents according to that(with applied pagination)
      if (title) {
        quiz = await this.Quiz.aggregate([
          { $match: { title: new RegExp('^' + req.query.title, 'i') } },
          { $sort: { createdAt: -1 } },
          { $skip: recordsPerPage * pageNumber - recordsPerPage },
          { $limit: recordsPerPage },
          {
            $lookup: {
              from: 'results',
              let: { quizId: '$_id' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$quizId', '$$quizId'] },
                        { $eq: ['$userId', resUserId] },
                      ],
                    },
                  },
                },
                {
                  $project: {
                    result: 1,
                    userScore: 1,
                    userPercentage: 1,
                    _id: 0,
                  },
                },
              ],
              as: 'resultdata',
            },
          },
        ]);
        totalRecords = await this.Quiz.find({
          title: new RegExp('^' + req.query.title, 'i'),
        }).count();
        totalPages = Math.ceil(totalRecords / recordsPerPage);
      } // else return all the documents (with applied pagination)
      else {
        quiz = await this.Quiz.aggregate([
          { $sort: { createdAt: -1 } },
          { $skip: recordsPerPage * pageNumber - recordsPerPage },
          { $limit: recordsPerPage },
          {
            $lookup: {
              from: 'results',
              let: { quizId: '$_id' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$quizId', '$$quizId'] },
                        { $eq: ['$userId', resUserId] },
                      ],
                    },
                  },
                },
                {
                  $project: {
                    result: 1,
                    userScore: 1,
                    userPercentage: 1,
                    _id: 0,
                  },
                },
              ],
              as: 'resultdata',
            },
          },
        ]);
      }

      quiz.map(q => {
        if (q.resultdata[0] != null) {
          q.result = q.resultdata[0].result;
          q.userScore = q.resultdata[0].userScore;
          q.userPercentage = q.resultdata[0].userPercentage;
          delete q.resultdata;
        } else {
          delete q.resultdata;
          return q;
        }
      });

      let message;
      let status;
      let dataLength = quiz.length;
      if (dataLength <= Number(process.env.NO_RECORDS)) {
        status = 204;
        message = i18N.errorMessages.noQuizAvailble;
      } else {
        status = 200;
        message = i18N.successMessages.dataFound;
      }
      let payload = {
        status: status,
        message: message,
        data: { quiz, totalPages: totalPages, totalRecords: totalRecords },
      };
      responseHandler(res, payload);
    } catch (error) {
      detectError(error, res);
    }
  };

  public getQuestions = async (req: Request, res: Response) => {
    try {
      const quiz = await QuizModel.findById({ _id: req.params.quizId });
      const questions = await QuestionModel.find({ quizId: req.params.quizId });
      const resUserId = new mongoose.Types.ObjectId(res.locals.userId);
      const resQuizId = new mongoose.Types.ObjectId(req.params.quizId)
      const result = await this.Result.findOne({ quizId: resQuizId, userId: resUserId });
      if ( questions.length<1 || !quiz) {
        let payload = {
          status: 400,
          message: i18N.errorMessages.noQuestionsAvailable,
          data: {},
        };
        errorHandler(res, payload);
      } else {
        if (result) {

          let payload = {
            status: 200,
            message: i18N.successMessages.alreadyAttempt,
            data: false,
          };
          responseHandler(res, payload);

        } else {
          let quizData = {
            title: quiz.title,
            duration: quiz.duration,
            description: quiz.description,
            questions: questions,
          };
          let payload = {
            status: 200,
            message: i18N.successMessages.getData,
            data: quizData,
          };
          responseHandler(res, payload);
        }
      }

    } catch (error) {
      let payload = {
        status: 500,
        message: i18N.errorMessages.serverError,
        error: { error: error },
      };
      errorHandler(res, payload);
    }
  };
}

export default QuizController;
