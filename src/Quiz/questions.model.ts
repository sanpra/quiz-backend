import mongoose from 'mongoose';
import { Question } from './questions.interface';
const Schema = mongoose.Schema;

const QuestionOptionSchema = new Schema({
  text: {
    type: String,
    trim: true,
    required: [true, 'Option text is required'],
  },
  isCorrect: {
    type: Boolean,
  },
});

const QuestionSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, 'Question title is required'],
      trim: true,
    },
    options: {
      type: [QuestionOptionSchema],
      required: true,
    },
    type: {
      type: String,
      enum: ['SingleSelect', 'MultiSelect'],
      default: 'SingleSelect',
    },
    quizId: {
      type: Schema.Types.ObjectId,
      ref: 'Quiz',
    },
  },
  { timestamps: true }
);

const QuestionModel = mongoose.model<Question & mongoose.Document>(
  'Question',
  QuestionSchema
);

export default QuestionModel;
