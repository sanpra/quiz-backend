export interface Question {
  title: String;
  options: QuestionOption;
  quizId: String;
}

export interface QuestionOption {
  text: String;
  isCorrect: Boolean;
}
