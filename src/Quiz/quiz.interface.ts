export default interface Quiz {
  title: String;
  description?: String;
  endDate: Date;
  duration: Number;
  createdBy: String;
  result?: String;
  userScore?: Number;
  userPercentage?: Number;
}
