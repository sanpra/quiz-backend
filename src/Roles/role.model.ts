import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const RoleSchema = new Schema(
  {
    name: {
      type: String,
      enum: ['Admin', 'User'],
      default: 'User',
    },
  },
  { timestamps: true }
);

const RoleModel = mongoose.model<mongoose.Document>('Role', RoleSchema);

export default RoleModel;
