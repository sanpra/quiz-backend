import express, { Request, Response } from 'express';

// response handler
import { responseHandler, detectError } from '../utils/handler';

// interfaces for controller
import Controller from '../common/controller.interface';

// models
import RoleModel from './role.model';
import { i18N } from '../utils/i18N';

class UserController implements Controller {
  public path = '/role';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes(): void {
    this.router.post(`${this.path}/add`, this.addRole);
  }

  public addRole = async (req: Request, res: Response) => {
    try {
      let newRole = new RoleModel(req.body);
      await newRole.save();
      let payload = {
        status: 200,
        message: i18N.successMessages.addOperationSuccess,
        data: {},
      };
      responseHandler(res, payload);
    } catch (error) {
      detectError(error, res);
    }
  };
}

export default UserController;
