import express, { Request, Response } from 'express';
import mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import 'dotenv/config';

// import all controllers here
import AuthController from './Auth/auth.controller';
import UserController from './User/user.controller';
import RoleController from './Roles/role.controller';
import QuizController from './Quiz/quiz.controller';
import ResultController from './Result/result.controller';

class App {
  public app: express.Application;
  public controllers = [
    new UserController(),
    new RoleController(),
    new AuthController(),
    new QuizController(),
    new ResultController(),
  ]; //keep adding controllers here to initialize them

  constructor() {
    this.app = express();
    this.initializeMiddlewares();
    this.connectDatabase();
    this.initializeControllers();
  }

  public connectDatabase(): void {
    mongoose.connect(process.env.MONGO_PATH, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    mongoose.Promise = global.Promise;
    mongoose.set('useCreateIndex', true);
    mongoose.connection.on('connected', () => {
      console.log('MongoDB is Connected');
    });
  }

  public initializeMiddlewares(): void {
    this.app.use(bodyParser.json());
    this.app.use(cors());
  }

  public initializeControllers(): void {
    this.controllers.forEach(controller => {
      this.app.use('/', controller.router);
    });
  }
}

export default new App().app;
